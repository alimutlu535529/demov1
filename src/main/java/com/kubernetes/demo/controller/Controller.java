package com.kubernetes.demo.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/demov1")
public class Controller {

    @GetMapping
    public ResponseEntity<String> getDemov1(){
        return ResponseEntity.of(Optional.of("demoV1"));
    }
}
